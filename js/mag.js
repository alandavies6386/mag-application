/**
 *	Data visualisations for MAG eye tracking project
 *	August 2015
 *	@author rob.dunne@manchester.ac.uk
 */

var app = angular.module('app', []);

app.controller('dataImage', function($scope, $http, $sce) {
	// Set vars
	
	// Data for the different views
	$scope.data = {
		'images': {
			1: { 
				'image': 'art/1934.2med.png', 
				'heatmap': 'viz/1934.2med_heatmap.jpg', 
				'title': 'Self-portrait, Louise Jopling R.B.A., 1877', 
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'n/a' 
			},
			2: { 
				'image': 'art/1995.184med.png',
				'heatmap': 'viz/1995.184med_heatmap.jpg',
				'title': 'Release, Mark Francis, 1994',
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'By permission of the artist Mark Francis'
			},
			3: { 
				'image': 'art/1976.79med_resized.png', 
				'heatmap': 'viz/1976.79med_heatmap.jpg', 
				'title': 'Sir Gregory Page-Turner, Pompeo Batoni, 1768', 
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'Manchester City Galleries' 
			},
			4: { 
				'image': 'art/1970.34med_resized.png', 
				'heatmap': 'viz/1970.34med_heatmap.jpg', 
				'title': 'Cheetah and Stag with Two Indians, George Stubbs, 1765',
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'Manchester City Galleries'
			},
			5: { 
				'image': 'art/1934.394med_resized.png',
				'heatmap': 'viz/1934.394med_heatmap.jpg',
				'title': 'When the West with Evening Glows, Joseph Farquharson R.A., 1901', 
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'Manchester City Galleries' 
			},
			6: { 
				'image': 'art/1917.170med_resized.png', 
				'heatmap': 'viz/1917.170med_heatmap.jpg', 
				'title': 'Rhyl Sands, David Cox, 1854', 
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'Manchester City Galleries' 
			},
			7: { 
				'image': 'art/1964.287med.png', 
				'heatmap': 'viz/1964.287med_heatmap.jpg', 
				'title': '14.6.64, John Hoyland, 1964', 
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'Courtesy of the Artist\'s estate / Bridgeman Art Library' 
			},
			8: { 
				'image': 'art/1947.445low.png', 
				'heatmap': 'viz/1947.445low_heatmap.jpg', 
				'title': 'Cheviot Farm, Frances Mary Hodgkins, 1939', 
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'n/a' 
			},
			9: { 
				'image': 'art/1968.173med.png', 
				'heatmap': 'viz/1968.173med_heatmap.jpg', 
				'title': 'Woman and Suspended Man, Thomas Sam Haile, 1939', 
				'text': 'Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'Gift of Mrs Marianne Haile' 
			}
		},
		'videos': {
			1: { 
				'image': '1934.2med.png', 
				'mp4': '1934.2med_reveal.mp4', 
				'title': 'Self-portrait gaze reveal', 
				'text': '1Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			},
			2: { 
				'image': '1976.79med_resized.png', 
				'mp4': '1976.79med_reveal.mp4', 
				'title': 'Sir Gregory Page-Turner gaze reveal', 
				'text': '2Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			},
			3: { 
				'image': '1934.394med_resized.png', 
				'mp4': '1934.394med_reveal.mp4', 
				'title': 'When the West with Evening Glows gaze reveal', 
				'text': '2Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			},
			4: { 
				'image': '1970.34med.jpg', 
				'mp4': '1970.34med_single.mp4', 
				'title': 'Cheetah and Stag with Two Indians single gaze', 'text': '1Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			},
			5: { 
				'image': '1970.34med.jpg', 
				'mp4': '1970.34med_multiple.mp4', 
				'title': 'Cheetah and Stag with Two Indians multiple gaze', 
				'text': '1Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			},
			6: { 
				'image': '1970.34med.jpg', 
				'mp4': '1970.34med_heatmap.mp4', 
				'title': 'Cheetah and Stag with Two Indians heatmap', 
				'text': '1Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			},
			7: { 
				'image': '1976.79med.jpg', 
				'mp4': '1976.79med_single.mp4', 
				'title': 'Sir Gregory Page-Turner single gaze', 'text': '1Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			},
			8: { 
				'image': '1976.79med.jpg', 
				'mp4': '1976.79med_multiple.mp4', 
				'title': 'Sir Gregory Page-Turner multiple gaze', 
				'text': '1Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			},
			9: { 
				'image': '1976.79med.jpg', 
				'mp4': '1976.79med_heatmap.mp4', 
				'title': 'Sir Gregory Page-Turner heatmap', 
				'text': '1Sed vel nisi sit amet sem eleifend consectetur. Morbi sit amet nibh quis risus tempor suscipit a quis arcu. Suspendisse id finibus justo. Vivamus mauris est, bibendum ac ipsum eu, malesuada convallis felis.', 
				'copyright': 'IAM Lab, University of Manchester' 
			}
		},
		'loading': true
	};

	// The current image
	$scope.image = {
		'src': '',
		'title': '',
		'text': '',
		'copyright': ''
	};
	
	// The current video
	$scope.video = {
		'title': '',
		'text': '',
		'copyright': '',
		'mp4': 'video/test.mp4',
		'image': ''
	};
	
	// Navigation object for the different views
	$scope.view = {
		'home': {
			'show': true
		},
		'images': {
			'show': false,
			'views': {
				'heatmap': false,
				'gazeplot': false
			},
			'currentFile': 1
		},
		'video': {
			'show': false,
			'currentFile': 1
		},
		'header': {
			'show': true
		}, 
		'menu': false
	};

	// Load the home page
	$scope.loadHome = function() {
		// Show the correct panel
		$scope.changeView(true, false, false, false);
		
		// Hide the header too
		$scope.view.header.show = false;
		
		// Animate the titles
		$('#mag-home h1').transition({ opacity: 1, delay: 500, y: '40px' });
		$('#mag-home h4').transition({ opacity: 1, delay: 1000, y: '-80px' });
		$('#start-arrow').transition({ opacity: 1, delay: 2000, y: '-50px' });
	};

	// Load the static image view
	$scope.loadImages = function(file) {
		// Keep it within range
		var filesLength = Object.size($scope.data.images);
		file = (!file || file < 1) ? 1: file;
		file = (file > filesLength) ? filesLength: file;

		// Show the correct panel
		$scope.changeView(false, true, false, false);

		// Update the navigation
		$scope.view.images.currentFile = file;

		// Add the image
		$scope.image.src = $scope.data.images[file].image;
		
		// Add the text
		$scope.image.title = $scope.data.images[file].title;
		$scope.image.text = $scope.data.images[file].text;
		$scope.image.copyright = $scope.data.images[file].copyright;
	};
	
	// Load the videos view
	$scope.loadVideo = function(file) {
		// Keep it within range
		var filesLength = Object.size($scope.data.images);
		file = (!file || file < 1) ? 1: file;
		file = (file > filesLength) ? filesLength: file;

		// Show the correct panel
		$scope.changeView(false, false, true, false);

		// Update the navigation
		$scope.view.video.currentFile = file;

		// Add the video src
		$scope.video.mp4 = $sce.trustAsResourceUrl("video/"+$scope.data.videos[file].mp4);
		
		// Add the poster image
		$scope.video.image = $scope.data.videos[file].image;
		
		// Add the text
		$scope.video.title = $scope.data.videos[file].title;
		$scope.video.text = $scope.data.videos[file].text;
		$scope.video.copyright = $scope.data.videos[file].copyright;
	};
	
	$scope.scrollToVideo = function() {
		$('html,body').animate({
          scrollTop: $('html').offset().top
        }, 1000);
	};
	
	// Change the navigation view
	$scope.changeView = function(home, view, video, anim) {
		$scope.view.home.show = home;
		$scope.view.images.show = view;
		$scope.view.video.show = video;
		
		// Show the header too
		$scope.view.header.show = true;
		
		// Hide the mobile menu
		$scope.view.menu = false;
	};
	
	// Swap between the static image views
	$scope.changeImagesView = function(view) {
		switch(view) {
			case 'plain':
				$scope.image.src = $scope.data.images[$scope.view.images.currentFile].image;
				break;
			case 'heatmap':
				$scope.image.src = $scope.data.images[$scope.view.images.currentFile].heatmap;
				break;
		}
	};
	
	// On page load start home page view
	$scope.loadHome();
});

// http://stackoverflow.com/questions/5223/length-of-a-javascript-object-that-is-associative-array
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

